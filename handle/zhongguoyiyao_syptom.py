import json
import re


class syptom():
    def __init__(self):
        self.file = "../data/中国医药信息查询平台/symptom_content.json"
        self.save_data = '../data/中国医药信息查询平台/'
        self.get_data = json.load(open(self.file, encoding='utf8'))['content']

    def get_syptom(self):
        f = open(self.save_data + "syptom.txt", 'w', encoding="utf8")
        for element in self.get_data:
            element = element['symptom']
            otherName = ''
            if element['otherName']:
                otherName = re.split("[、，]", element['otherName'])
            f.write(element['chineseName'] + '\t' + '\t'.join(otherName) + '\n')

    def get_sypto(self):
        # f = open(self.save_data + "syptom_dis.txt", 'w', encoding="utf8")
        for element in self.get_data:
            element = element['symptom']
            dis = [i for i in re.split("<p>(.*?)</p>",element['commonDiseases']) if i !='']
            if len(dis)==1:
                dis = re.split("[、，]", dis[0].replace("等","").replace("。",""))
                # print(dis)
            else:
                print(dis)
                # f.write(element['chineseName'] + '\t' + '\t'.join(dis) + '\n')
    def get_sypto_dis(self):
        # f = open(self.save_data + "syptom_dis_cause.txt", 'w', encoding="utf8")
        for element in self.get_data:
            element = element['symptom']
            dis = [re.sub('[与|有关|\u200b|等|主要|引起|主要是|]','',i) for i in re.split("、",element['mainCauses']) if i !='']
            print(dis,element['mainCauses'])

            # f.write(element['chineseName']+"\t"+"\t".join(dis)+"\n")
            # if len(dis)==1:
            #     dis = re.split("[、，]", dis[0].replace("等","").replace("。",""))
            #     # print(dis)
            # else:
            #     print(dis)
                # f.write(element['chineseName'] + '\t' + '\t'.join(dis) + '\n')
    def get_sypto_inspect(self):
        f = open(self.save_data + "syptom_inspectionItems.txt", 'w', encoding="utf8")
        for element in self.get_data:
            element = element['symptom']
            res =[i for i in element['inspectionItems'].replace("等",'').replace("\u200b",'').replace('“','').replace('"','').split("、") if i!='']
            f.write(element['chineseName'] + '\t' + '\t'.join(res) + '\n')
    def get_parts(self):
        f = open(self.save_data + "syptom_parts.txt", 'w', encoding="utf8")
        for element in self.get_data:
            element = element['symptom']
            res = element['parts']
            if res:
                res =res.replace("其他","").replace("等","")
                res =[i for i in re.split("[、|,|，]",res) if i !='']
                print(res)
                f.write(element['chineseName'] + '\t' + '\t'.join(res) + '\n')
    def get_drugTherapy(self):
        f = open(self.save_data + "syptom_drugTherapy.txt", 'w', encoding="utf8")
        for element in self.get_data:
            element = element['symptom']
            res = element['drugTherapy']
            if res:
                res =re.sub(re.compile("</p>|<p>|<sub>1</sub>|。|等|<sub>2</sub>|<sub>12</sub>|</sub>|;|<sub>|"),'',res)
                # res = res.replace("其他", "").replace("等", "")
                res = [i for i in re.split("[、|,|，]", res) if i != '']
                print(res)
                f.write(element['chineseName'] + '\t' + '\t'.join(res) + '\n')

syptom().get_drugTherapy()
