import json
import re
import os


class bianMing():
    def __init__(self):
        self.save_path = "../data/便民查询网/"
        self.handle_path = "../handle_data/便民查询网/"

    def handle(self):
        f_name = open(self.handle_path + "name.txt", "w", encoding="utf8")
        f_detail = open(self.handle_path + "detail.txt", "w", encoding="utf8")
        with open(self.save_path + "便民查询网_.json", encoding='utf8') as data_json:
            data_json = data_json.read()
            data_json = json.loads(data_json)['content']
            for element in data_json:
                temp = [element_one.replace('[', '').replace(']', '') for element_one in
                        re.findall("\[.*?\]", element['名字'])]
                if temp:
                    writer = re.sub("\[.*?\]", "", element['名字']) + "\t" + '\t'.join(re.split("[，]", temp[0])) + "\n"
                    # print(writer)
                    f_name.write(writer)
                else:
                    f_name.write(element['名字'] + "\n")
                for key, value in element['营养成分'].items():
                    number = float(re.findall(r'-?\d+\.?\d*e?-?\d*?', value)[0])
                    if number != 0.0:
                        print(number)


bianMing().handle()
