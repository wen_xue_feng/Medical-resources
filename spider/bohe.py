import json
import re
import requests
from bs4 import BeautifulSoup


def single_bohe(url):
    """
    基于薄荷网获取单个食物卡路里
    :param url: 
    :return:返回json包
    """
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Connection": "keep-alive",
        "Host": "m.boohee.com",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36"}
    response = requests.get(url, headers=headers)
    result ={}
    soup = BeautifulSoup(response.text, 'html.parser')
    for element in soup.find_all(id='foodBrief'):
        for element_one in element.find_all('img'):
            result['食物']=element_one['all']
            result['图片'] = element_one['src']
        for element_two in element.find_all('p'):
            result['总热量'] = re.sub(('[\n \r \t  ：\xa0\u3000]'), '', element_two.get_text())
    key_ =[]
    value_=[]
    for element in soup.find_all(id='foodData'):
            for element_two in element.find_all('tr'):
                for element_three in element_two.find_all('th'):
                    key_.append(element_three.get_text())
                for element_three in element_two.find_all('td'):
                    value_.append(element_three.get_text())
    result['热量组成'] =dict(zip(key_,value_))
    return result

url ='http://m.boohee.com/foods/heidou'
print(single_bohe(url))