import re
import requests
from bs4 import BeautifulSoup
import pymysql
import uuid
from datetime import datetime


class spiderWenJuanWang():
    """
    爬取问卷网
    """

    def __init__(self):
        self.db = pymysql.connect('')
        self.headers = {
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'zh-CN,zh;q=0.9',
            'cache-control': 'max-age=0',
            'connection': 'keep-alive',
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}

    def get_single(self, url):
        """
        爬取单页信息
        :param url:
        :return:
        """
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        title = ''.join([element.get_text() for element in soup.find_all(class_='answer-title one-line-ellipsis')])
        soup = str(soup).split("question-content")
        result = []
        for element in soup:
            temp = {}
            element = BeautifulSoup(element, 'html.parser')
            status = "1"
            for element_one in element.find_all(class_='question-title'):
                temp_text = re.sub("[*\n\t\r ]", "", str(element_one.get_text()))
                temp_text = temp_text[temp_text.find("：")+1:]
                temp["问题"] = temp_text
                if "多选" in temp_text:
                    status = "2"
                elif "填空" in temp_text :
                    status = "3"
                else:
                    status = "1"
            answer = []
            for element_four in element.find_all("lable"):
                answer.append(re.sub("[*\n\t\r ]", "", str(element_four.get_text())))
            temp["答案"] = answer
            if len(answer)==1:
                status = "3"
            temp['type'] = status
            if "问题" in temp and temp['问题']!='生日' and temp['问题'].find("报名信息")==-1:
                result.append(temp)
        return title, result

    def to_sql(self, url, name=None, gender=False, age=False):
        """
        问卷到数据库
        :param url: 网址
        :param name: 名字
        :param gender: 姓名
        :param age: 年龄
        :return:
        """
        letter = {"0": "A", "1": "B", "2": "C", "3": "D", "4": "E", "5": "F", "6": "G", "7": "H", "8": "I", "9": "J", "10": "K", "11": "L", "12": "M", "13": "N", "14": "O", "15": "P", "16": "Q", "17": "R", "18": "S", "19": "T", "20": "U"}
        db = self.db
        conn = db.cursor()
        title, result = self.get_single(url)
        if name is not None:
            title = name
        # 创建模板id_form
        form_id = str(uuid.uuid1())
        sql = "insert into dw_form (organization,department,doctor_id,type_id,form_name,sequence,create_time,update_time,wjbm,yljgdm) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (
            "42502657200", "01", "13", "1", title, "00000000", datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            datetime.now().strftime('%Y-%m-%d %H:%M:%S'), form_id, "42502657200")
        conn.execute(sql)
        id_form = str(conn.lastrowid)
        # 创建性别
        if gender:
            sql = "insert into dw_question (question_format,question_option,question_title,form_id,sequence,tmbm,xxlx,xszt,xgbz,tmscsj,sjscsj,wjbm) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (
                "1", "男^&^A!@!女^&^B", "性别", id_form, "0000000001", str(uuid.uuid1()), "1", "1", "1",
                datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                datetime.now().strftime('%Y-%m-%d %H:%M:%S'), form_id)
            print(sql)
            conn.execute(sql)
            db.commit()
        # 创建年龄
        if age:
            sql = "insert into dw_question (question_format,question_option,question_title,form_id,sequence,tmbm,xxlx,xszt,xgbz,tmscsj,sjscsj,wjbm,tktms,jsbz) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (
                99, "", "年龄", id_form, "0000000001", str(uuid.uuid1()), "1", "1", "1",
                datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                datetime.now().strftime('%Y-%m-%d %H:%M:%S'), form_id,1,0)
            print(sql)
            conn.execute(sql)
        # 创建模板中的问题
        for element in result:
            answ = element['答案']
            res_answer = []
            for i in range(len(answ)):
                res_answer.append(answ[i] + "^&^" + letter[str(i)])
            res_answer = '!@!'.join(res_answer)
            if element['type']=="3":
                sql = "insert into dw_question (question_format,question_option,question_title,form_id,sequence,tmbm,xxlx,xszt,xgbz,tmscsj,sjscsj,wjbm,tktms,jsbz) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (
                    99, '', element['问题'], id_form, "0000000001", str(uuid.uuid1()), "1", "1", "1",
                    datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    datetime.now().strftime('%Y-%m-%d %H:%M:%S'), form_id,1,0)
            else:
                sql = "insert into dw_question (question_format,question_option,question_title,form_id,sequence,tmbm,xxlx,xszt,xgbz,tmscsj,sjscsj,wjbm) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (
                    element['type'], res_answer, element['问题'], id_form, "0000000001", str(uuid.uuid1()), "1", "1", "1",
                    datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    datetime.now().strftime('%Y-%m-%d %H:%M:%S'), form_id)
            print(sql)
            conn.execute(sql)
        # 提交数据库
        db.commit()


if __name__ == '__main__':
    print(spiderWenJuanWang().to_sql("https://www.wenjuan.com/lib_detail_full/55a87c20f7405b2111bb7ace", "高血压患病风险评估",
                                     age=True, gender=True))
    print(spiderWenJuanWang().to_sql("https://www.wenjuan.com/lib_detail_full/5c2486c6a320fc70e58a0206",
                                     '老年骨质疏松患者骨折的高危因素评估'))
    print(
        spiderWenJuanWang().to_sql("https://www.wenjuan.com/lib_detail_full/53744b81f7405b2011a7325a", name='糖尿病危险因素问卷')) # 第三题的生日，不知道改为填空题
    print(spiderWenJuanWang().to_sql("https://www.wenjuan.com/lib_detail_full/5991774aa320fc6608815f41","高血脂人群健康调查问卷",age=True,gender=True))#