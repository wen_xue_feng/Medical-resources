import re
import requests
from bs4 import BeautifulSoup
import json
import pymysql
class Config:
    BYTES_HOST = "61.171.14.77"
    BYTES_PORT = 3306
    BYTES_USER = "root"
    BYTES_PASSWORD = "!Aa123456"
    BYTES_DATABASE = "daozhen"
class xinXueGuan():
    def __init__(self):
        self.main_url = ['http://www.heartabc.com/xxgbdq/xxgnkcjjb/#1', 'http://www.heartabc.com/xxgbdq/xxgnkqtjb/',
                         'http://www.heartabc.com/xxgbdq/xxwkqtjb/', 'http://www.heartabc.com/xxgbdq/xxwkcjjb/']
        self.headers = {
            # 'Host': 'www.heartabc.com',
            # 'Sec-Fetch-Dest': 'document',
            # 'Sec-Fetch-Mode': 'navigate',
            # 'Sec-Fetch-Site': 'same-origin',
            # 'Sec-Fetch-User': '?1',
            # 'Upgrade-Insecure-Requests': '1',
            # 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            # 'Accept-Encoding': 'gzip, deflate, br',
            # 'Accept-Language': 'zh-CN,zh;q=0.9',
            # 'Cache-Control': 'max-age=0',
            # 'Connection': 'keep-alive',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
        self.save_path = "../data/就医助手/"

    def get_single(self):
        result = []

        for i in range(1, 17):
            url = "http://yyk.39.net/haikou/hospitals/c_p" + str(i) + "/"
            response = requests.get(url, headers=self.headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            # content = ''.join([str(element) for element in soup.find_all(class_='hospitalfulllist')]).split("li")
            for element in soup.find_all(class_='hospitalfulllist'):
                for element_one in element.find_all("li"):
                    temp = {}
                    for element_two in element_one.find_all("a"):
                        for element_four in element_two.find_all("img"):
                            temp["image"] = element_four['src']
                        temp['url'] = "http://yyk.39.net" + element_two['href']
                        temp['name'] = element_two.get_text()
                    for element_three in element_one.find_all("p"):
                        text = element_three.get_text()
                        if text.find("医院地址") != -1:
                            temp['地址'] = text.replace("医院地址：", "")
                    result.append(temp)
            with open(self.save_path + 'data.json', "w", encoding="utf8") as dump_f:
                json.dump({"result": result}, dump_f, ensure_ascii=False, indent=2)

    def get_department(self, url):
        result =[]
        for i in range(1, 10):
            count = 0
            try:
                response = requests.get(url+"?page="+str(i), headers=self.headers)
                soup = BeautifulSoup(response.text, 'html.parser')
                for element in soup.find_all(class_='doctortable'):
                    for element_one in element.find_all("li"):
                        temp = {}
                        status = 0
                        # print(element_one)
                        for element_two in element_one.find_all("a"):
                            if status > 1:
                                continue
                            for element_four in element_two.find_all("img"):
                                temp["image"] = element_four['src']
                            temp['name'] = element_two.get_text()
                            status = status + 1
                        text_temp = []
                        for element_three in element_one.find_all("p"):
                            text = element_three.get_text()
                            text_temp.append(text)
                            if text.find("擅长疾病") != -1:
                                temp['擅长疾病'] = text.replace("擅长疾病：", "")
                            if len(text_temp) == 2:
                                temp['职称'] = text_temp[0]
                                temp_text = text_temp[1].split(" ")
                                if len(temp_text) > 1:
                                    temp['医院'] = temp_text[0]
                                    temp['科室'] = temp_text[1]
                            else:
                                temp_text = text_temp[0].split(" ")
                                if len(temp_text) > 1:
                                    temp['医院'] = temp_text[0]
                                    temp['科室'] = temp_text[1]
                        if temp:
                            count = count + 1
                            result.append(temp)
            except:
                print(url+"fail")
            if count == 0:
                continue
        return result
    def get_all(self):
        result =[]
        with open(self.save_path + 'data.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)['result']
            for element in data_json:
                url = element['url'].replace(".html","_doctors.html").replace("hk/zhuanke","hospital").replace("hk/zonghe","hospital")
                print(url)
                result.extend(self.get_department(url))
        with open(self.save_path + 'doctor.json', "w", encoding="utf8") as dump_f:
            json.dump({"result": result}, dump_f, ensure_ascii=False, indent=2)

    def to_sql(self):
        conn1 = pymysql.connect(host=Config.BYTES_HOST, port=Config.BYTES_PORT, user=Config.BYTES_USER,
                                                      password=Config.BYTES_PASSWORD, database=Config.BYTES_DATABASE)
        conn = conn1.cursor()
        count = 0
        # with open(self.save_path + 'doctor.json', encoding='utf8') as data_json:
        #     data_json = json.load(data_json)['result']
        #     for element in data_json:
        #         try:
        #             sql = "insert into kh_doctor (id,deptName,doctName,doctTile,hosName,doctInfo) values ('%s','%s','%s','%s','%s','%s')" % (
        #                 "42500049500sk13388"+str(count),element['科室'],element['name'],element['职称'],element['医院'],element['擅长疾病'])
        #             print(sql)
        #             conn.execute(sql)
        #             count =count+1
        #             conn1.commit()
        #         except:
        #             pass
        with open(self.save_path + 'data.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)['result']
            for element in data_json:
                 if element['name'].find("服务")==-1:
                    try:
                        sql = "insert into kh_hospital (id,hosName,hospitalAdd) values ('%s','%s','%s')" % (
                            "42500049500sk13388"+str(count),element['name'],element['地址'])
                        print(sql)
                        conn.execute(sql)
                        count =count+1
                        conn1.commit()
                    except:
                        pass



# 风心病 扩张型心肌病
# print(xinXueGuan().get_department("http://yyk.39.net/hospital/1dc92_doctors.html"))
xinXueGuan().to_sql()
